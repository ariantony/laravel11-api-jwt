<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HotelTransactionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\LinenCircleController;


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:api')->name('logout');
    Route::post('/refresh', [AuthController::class, 'refresh'])->middleware('auth:api')->name('refresh');
    Route::post('/me', [AuthController::class, 'me'])->middleware('auth:api')->name('me');

    // Route::post('/users/{user_id}/roles/{role_name}', [AuthController::class, 'assignUserToRole'])->name('users.roles.assign');
    Route::post('/users/assign-user-to-role', [AuthController::class, 'assignUserToRole'])->name('users.roles.assign');
    Route::post('/users/assign-permission-to-user', [AuthController::class, 'assignPermissionToUser'])->name('users.permissions.assign');
    Route::post('/users/assign-permission-to-role', [AuthController::class, 'assignPermissionToRole'])->name('roles.permissions.assign');

    Route::delete('/roles/{role_name}/permissions/{permission_name}', [AuthController::class, 'removePermissionFromRole'])->name('roles.permissions.remove');
    Route::delete('/users/{user_id}/roles/{role_name}', [AuthController::class, 'removeUserFromRole'])->name('users.roles.remove');
    Route::delete('/users/{user_id}/permissions/{permission_name}', [AuthController::class, 'removePermissionFromUser'])->name('users.permissions.remove');

    // Route::post('/roles', [AuthController::class, 'createRole'])->name('roles.create');
    // Route::post('/permissions', [AuthController::class, 'createPermission'])->name('permissions.create');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'permissions'
], function ($router) {
    Route::resource('roles', RoleController::class)->except(['create', 'edit']);
    Route::resource('permissions', PermissionController::class)->except(['create', 'edit']);
});

// Route::group(['middleware' => [\Spatie\Permission\Middleware\RoleOrPermissionMiddleware::using(['manager', 'edit articles'])]], function () { ... });

// Route::group(['middleware' => [\Spatie\Permission\Middleware\RoleMiddleware::using('admin')]], function ($router) {
//     Route::resource('roles', RoleController::class)->except(['create', 'edit']);
//     Route::resource('permissions', PermissionController::class)->except(['create', 'edit']);
// });

Route::group([
    'middleware' => 'api',
    'prefix' => 'linen-circle'
], function ($router) {
    Route::resource('linen-circles', LinenCircleController::class);
    Route::get('/hotel-transactions/status-types', [HotelTransactionController::class, 'status_type']);
    Route::resource('hotel-transactions', HotelTransactionController::class);
});
