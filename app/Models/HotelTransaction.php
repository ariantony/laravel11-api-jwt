<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HotelTransaction extends Model
{
    use HasFactory;
    protected $table = 'hospital_transactions';

    public static function getIdTransaction(): string
    {
        $user_id = auth()->user()->id;

        $hospitalCode = DB::table('users')
            ->join('user_management', 'users.id', '=', 'user_management.users_id')
            ->join('hospitals', 'user_management.hospital_id', '=', 'hospitals.id')
            ->where('users.id', $user_id)
            ->whereNull('user_management.deleted_at')
            ->select('hospitals.hospital_code')
            ->first()->hospital_code;

        $wa_code = DB::table('users as u')
            ->join('user_management as um', 'um.users_id', '=', 'u.id')
            ->join('work_areas as wa', 'wa.id', '=', 'um.work_area_id')
            ->where('u.id', 17)
            ->whereNull('um.deleted_at')
            ->select('wa.code')
            ->first()->code;

        $date = date('Ymd'); // Get the date in YYYYMMDD format
        $randomNumber = rand(1000, 9999); // Generate a random number between 1000 and 9999
        return 'HS' . $wa_code . $hospitalCode . $date . $randomNumber; // Combine the date and random number
    }
}
