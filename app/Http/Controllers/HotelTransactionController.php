<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserManagement;
use App\Models\HotelTransaction;
use Illuminate\Support\Facades\DB;

class HotelTransactionController extends Controller
{
        /**
         * Display a listing of the resource.
         */
        public function index(Request $request) {
        $perPage = $request->input('per_page', 10); // Default to 10 per page

        $hotel_transactions = HotelTransaction::paginate($perPage); // Use paginate()

        return response()->json([
            'hotel_transactions' => $hotel_transactions,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function status_type()
    {

        if (!auth()->check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user_id = auth()->user()->id;
        // dd($user_id);

        $statusType = DB::table('user_management as um')
            ->join('work_areas as wa', 'wa.id', '=', 'um.work_area_id')
            ->join('users as u', 'u.id', '=', 'um.users_id')
            ->join('type_of_cycle_statuses as cs', 'cs.work_areas_id', '=', 'wa.id')
            ->where('u.id', $user_id)
            ->whereNull('um.deleted_at')
            ->orderBy('cs.id','asc')
            ->select('cs.name', 'cs.id')
            ->get()
            ->pluck('name', 'id')
            ->toArray();

        return response()->json([
            'status_type' => $statusType,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        dd($request);
        // Pengecekan sudah login atau belum

        if (!auth()->check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user_id = auth()->user()->id;

        $user_profile = UserManagement::where('users_id', $user_id)
        ->where('status', true)
        ->first();
        $hotelTransaction = new HotelTransaction;

        // Fill the HotelTransaction with validated data

        $user_id = auth()->user()->id;
        // dd($user_id);


        $hotelTransaction->code = HotelTransaction::getIdTransaction();
        $hotelTransaction->work_area_id = $user_profile->work_area_id;
        $hotelTransaction->hospital_id = $user_profile->hospital_id;
        $hotelTransaction->user_id = $user_id;
        $hotelTransaction->type_of_cycle_status_id = $request->input('type_of_cycle_status_id');

        // dd($hotelTransaction);

        // Save the HotelTransaction to the database
        $hotelTransaction->save();

        // Return a success response with the newly created HotelTransaction
        return response()->json([
            'message' => 'Hotel Transaction created successfully',
            'data' => $hotelTransaction,
        ], 201); // 201 Created status code
    }

    /**
     * Display the specified resource.
     */
    public function show(HotelTransaction $hotelTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, HotelTransaction $hotelTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(HotelTransaction $hotelTransaction)
    {
        //
    }
}
