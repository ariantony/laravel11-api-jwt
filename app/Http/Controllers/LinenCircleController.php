<?php

namespace App\Http\Controllers;

use App\Models\LinenCircle;
use Illuminate\Http\Request;

class LinenCircleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $perPage = $request->input('per_page', 10); // Default to 10 per page

        $linenCircles = LinenCircle::paginate($perPage); // Use paginate()

        return response()->json([
            'linen_circles' => $linenCircles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate the request data
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
            'hospital_transaction_id' => 'required|integer',
            'type_of_cycle_status_id' => 'required|integer',
            'tag_id' => 'required|integer',
            'user_id' => 'required|integer',
            'status' => 'required|string|in:pending,processing,completed', // Assuming status is one of these
        ]);

        // Create a new LinenCircle instance
        $linenCircle = new LinenCircle;

        // Fill the LinenCircle with validated data
        $linenCircle->name = $request->input('name');
        $linenCircle->description = $request->input('description');
        $linenCircle->hospital_transaction_id = $request->input('hospital_transaction_id');
        $linenCircle->type_of_cycle_status_id = $request->input('type_of_cycle_status_id');
        $linenCircle->tag_id = $request->input('tag_id');
        $linenCircle->user_id = $request->input('user_id');
        $linenCircle->status = $request->input('status');

        // Save the LinenCircle to the database
        $linenCircle->save();

        // Return a success response with the newly created LinenCircle
        return response()->json([
            'message' => 'Linen Circle created successfully',
            'linen_circle' => $linenCircle,
        ], 201); // 201 Created status code
    }

    /**
     * Display the specified resource.
     */
    public function show(LinenCircle $linenCircle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LinenCircle $linenCircle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LinenCircle $linenCircle)
    {
        //
    }
}
