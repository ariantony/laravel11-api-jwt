<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register() {
        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = new User;
        $user->name = request()->name;
        $user->email = request()->email;
        $user->password = bcrypt(request()->password);
        $user->save();

        return response()->json($user, 201);
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $token =  $this->respondWithToken($token);
        $user = auth()->user(); // Get the authenticated user
        // Get roles and permissions
        $roles = $user->roles->map(function ($role) {
            return [
                'id' => $role->id,
                'name' => $role->name,
            ];
        });
        $permissions = $user->getAllPermissions()->map(function ($permission) {
            return [
                'id' => $permission->id,
                'name' => $permission->name,
                'pivot' => $permission->pivot,
            ];
        });
        return response()->json([
            'token' => $token,
            'user' => $user->name,
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user(); // Get the authenticated user

        // Get roles and permissions
        $roles = $user->roles->map(function ($role) {
            return [
                'id' => $role->id,
                'name' => $role->name,
            ];
        });
               // $permissions = $user->permissions->pluck('name'); // Get permission names as an array
        $permissions = $user->getAllPermissions()->map(function ($permission) {
            return [
                'id' => $permission->id,
                'name' => $permission->name,
                'pivot' => $permission->pivot,
            ];
        });

        return response()->json([
            'user' => $user->name,
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
       $token = $this->respondWithToken(auth()->refresh());

        $user = auth()->user(); // Get the authenticated user

        // Get roles and permissions
        $roles = $user->roles->map(function ($role) {
            return [
                'id' => $role->id,
                'name' => $role->name,
            ];
        });
        $permissions = $user->getAllPermissions()->map(function ($permission) {
            return [
                'id' => $permission->id,
                'name' => $permission->name,
                'pivot' => $permission->pivot,
            ];
        });
        return response()->json([
            'token' => $token,
            'user' => $user->name,
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

        // // 1. Create Role
        // public function createRole(Request $request)
        // {
        //     $request->validate([
        //         'name' => 'required|string|unique:roles',
        //     ]);

        //     $role = Role::create(['name' => $request->name]);

        //     return response()->json([
        //         'message' => 'Role created successfully',
        //         'role' => $role,
        //     ], 201);
        // }

        public function assignPermissionToRole(Request $request)
        {
            $request->validate([
                'role_id' => 'required|integer',
                'permission_names' => 'required|array',
                'permission_names.*' => 'required|string',
            ]);

            $role = Role::findOrFail($request->role_id);

            // Loop through the provided permission names
            foreach ($request->permission_names as $permissionName) {
                $permission = Permission::where('name', $permissionName)->firstOrFail();
                $role->givePermissionTo($permission);
            }

            return response()->json([
                'message' => 'Permissions assigned to role successfully',
            ], 200);
        }

        public function removePermissionFromRole(Request $request)
        {
            $request->validate([
                'role_name' => 'required|string',
                'permission_name' => 'required|string',
            ]);

            $role = Role::where('name', $request->role_name)->firstOrFail();
            $permission = Permission::where('name', $request->permission_name)->firstOrFail();

            $role->revokePermissionTo($permission);

            return response()->json([
                'message' => 'Permission removed from role successfully',
            ], 200);
        }

        // 4. Create Permission
        public function createPermission(Request $request)
        {
            $request->validate([
                'name' => 'required|string|unique:permissions',
            ]);

            $permission = Permission::create(['name' => $request->name]);

            return response()->json([
                'message' => 'Permission created successfully',
                'permission' => $permission,
            ], 201);
        }

        public function assignUserToRole(Request $request)
        {
            $request->validate([
                'user_id' => 'required|integer',
                'role_name' => 'required|string',
            ]);

            $user = User::findOrFail($request->user_id);
            $role = Role::where('name', $request->role_name)->firstOrFail();

            $user->assignRole($role);

            return response()->json([
                'message' => 'User assigned to role successfully',
            ], 200);
        }

        public function assignPermissionToUser(Request $request)
        {
            // 1. Authentication Check
            if (!auth()->check()) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            // 2. Permission Check (Optional but recommended)
            //  - You can add a check here to ensure the authenticated user has the
            //    permission to assign permissions to others.

            // 3. Get the User to Assign Permissions To
            $user = User::findOrFail($request->user_id); // Use the provided user_id

            // 4. Assign Permissions
            foreach ($request->permission_names as $permissionName) {
                $permission = Permission::where('name', $permissionName)->firstOrFail();
                $user->givePermissionTo($permission);
            }

            return response()->json([
                'message' => 'Permissions assigned to user successfully',
            ], 200);
        }

        public function removeUserFromRole(Request $request)
        {
            $request->validate([
                'user_id' => 'required|integer',
                'role_name' => 'required|string',
            ]);

            $user = User::findOrFail($request->user_id);
            $role = Role::where('name', $request->role_name)->firstOrFail();

            $user->removeRole($role);

            return response()->json([
                'message' => 'User removed from role successfully',
            ], 200);
        }

        public function removePermissionFromUser(Request $request)
        {
            $request->validate([
                'user_id' => 'required|integer',
                'permission_name' => 'required|string',
            ]);

            $user = User::findOrFail($request->user_id);
            $permission = Permission::where('name', $request->permission_name)->firstOrFail();

            $user->revokePermissionTo($permission);

            return response()->json([
                'message' => 'Permission removed from user successfully',
            ], 200);
        }
}
